# Regular expressions (ou regex)
Allows to find a set of character (a given pattern) in a text (a list of character strings).

Here I explain the use of [Perl's definition of regular expressions](http://perldoc.perl.org/perlretut.html#Using-character-classes) in R. Perl and R definitions of regular expressions are extremely similar, but R can be [a bit buggy](http://stackoverflow.com/questions/28730387/difference-between-0-9-n-times-and-0-9n-in-r-regexp). Fortunately, using perl regular expressions is as simple as passing the `perl=TRUE` argument to functions like `grep`, `grepl` or `gsub`.  
ex: `gsub(".*([a-z]@[a-z].com).*","\\1",fields,perl=TRUE)` will make a list of all the emails in "fields" using perl regex. 

For official documentation on regular expressions on R: `?regex`.  
In bash grep, you can (should) also use perl regular expressions using `grep -P`

## Escaping special characters
`\` everything in regular expressions is about using special characters like `?` `.` etc having a special meaning. If you want to match these characters you need to add before: `\` in perl or `\\` R as R eats one when passing it to perl. 
ex: `myFile\\.csv` matches "myFile.csv" but not "myFile_csv". `myFile.csv` would match both (see next section).

## Matching alternate possibilities (OR)
`.` anything ex: `a.c` matches "abc", "aec" "a.c" ...

`[]` Defining alternate characters ex: `[aA]` matches "a" or "A"  
within `[]` `-` means from-to ex: `[a-z]` matches "a", "b", ... "z" and  `[0-9]` matches "0","1",...,"9" 

`|` matches this OR that ex: `dog|cat` matches "dog" or "cat"

`^` within `[]` means "not" ex: [^a] matches any character but "a"

Note 1: ignoring upper case in R is better done by using first `tolower(myString)`  
Note 2: ignoring accents (ou cédilles...) in R is better done by using first:  
`iconv(myString, to='ASCII//TRANSLIT')`


## Quantifiers
`?` the character before should be present 0 or 1 time

`*` the character before can be absent or present any number of times

`+` the character before before should be present 1 or more times  
ex: `da+` matches "da" or "d"

`{n}` the character before exactly n times ex: [ab]{3} means a or b 3 times.  
Inside `{}` a `,` means "from,to"   
ex1: `[ab]{2,4}` means a or b 2 to 4 times  
ex1: `[ab]{2,}` means a or b 2 or more times

This actually doesn't work in R: "!" means not something  
ex: "!a" would match "b", "cd", but not "ab"

## Groups
`()` defines groups of things. It allow for example to reuse them:  
ex1: in R, `gsub("(ab?c)de","S\\1E",a,perl=TRUE)` will replace  
"acde" by "SacE" and replace "abcde" by "SabcE".  
ex2: `gsub("bulletin_([0-9]{4})-([01][0-9])-([0-2][0-9]).csv","year:\\1;month:\\2;day:\\3",fileNameList,perl=TRUE)` replaces "bulletin_2016-10-02.csv" by "year:2016;month:10;day:02". 

It is also nice to combine `()` with `|` when things get really complex  
ex: `the (dog|cat) (eats|devours)` matches "the dog eats", "the dog devours" and "the cat devours"  but not "the dog defecates" nor "the elephant eats"

Quantifiers can also apply to the groups:  
ex: to match a four digit year in a file name whether or not you have month/day after it: 
```
fileNames <- c("Alsace-Bretagne_2015-08-15_2016-08-14.csv","meligethe_2011_2012.csv")
pattern <- paste0(".*([0-9]{4})(-[0-9]{2}-[0-9]{2})?\\.csv")
gsub(pattern,"\\1",fileNames)
```
returns: 2016,2012

## Specify position in string
`^` means start of the character string ex: `^abc` will match "abc" but not "aabc". "abc" matches both.

`$` means end of the character string ex: `abc$` will match "abc" but not "abcd". "abc" matches both.

`\\<` means the beginning of a word ex: `\\<dog` will match "dog" and "the dog" but not "thedog"

`\\>` means the end of a word 

## Some more examples to try in R
```
a <- c("abcdaa","abcda","adcda","abdac","abdda","aabcdaa")
grep("ab[cd]da.*",a,perl=TRUE)
grep("ab[cd]da.+",a,perl=TRUE)
grep("[ab]{2}[cd]da",a,perl=TRUE)
fileNameList <- c("bulletin_2012-03-24.csv","trash_2014-04-24.csv","bulletin_201224.csv")
gsub("bulletin_([0-9]{4})-([01][0-9])-([0-2][0-9]).csv","year:\\1;month:\\2;day:\\1",fileNameList,perl=TRUE)
gsub("dog|cat","pet",c("dog","elephant","cat"),perl=TRUE)
```